"use strict";

document.addEventListener("DOMContentLoaded", setup);

//global var that keeps track of all the notes
let toDoList = [];

function setup(){
    const btn = document.getElementById('postBtn');
    btn.addEventListener("click", submit);
}

function submit(){
    //task title input
    let task = document.getElementById('task_textbox').value;
    
    //description of the task
    let description = document.getElementById('description').value;
    
    //importance of the task from slider
    let importance = document.getElementById('slider').value;
    
    //find the correct way to find the value of the checked button
    let category = document.getElementsByTagName('category');
    let checkValue;
    for (var i = 0; i < category.length; i++){
        if (category[i].checked){
            checkValue = category[i].value;
            break;
        }
    }

    let toDo = {
        "taskName" : task,
        "taskDescription" : description,
        "taskImportance" : importance,
        "taskCategory" : checkValue
    };

    toDoList.push(toDo);
    makeNote(toDo);
}

function makeNote(toDo){
    //section will be a new task
    let note = document.createElement("section");
    note.setAttribute("id", "note");
    
    /*creates the title of the task based on input,
    needs formatting but works*/
    let nameLine = document.createElement("p");
    nameLine.setAttribute("id", "name");
    let name = document.createTextNode(toDo["taskName"]);
    nameLine.appendChild(name);
    note.appendChild(nameLine);

    /*need to find a way to print stars,
    got the right value*/
    let importanceLine = document.createElement("p");
    let importance = document.createTextNode(toDo["taskImportance"]);
    importanceLine.appendChild(importance);
    note.appendChild(importanceLine);

    /*will show the description of the task based on input,
    works but might need styling*/
    let descriptionLine = document.createElement("p");
    let desc = document.createTextNode(toDo["taskDescription"]);
    descriptionLine.appendChild(desc);
    note.appendChild(descriptionLine);

    /*find correct way to get value of checked button,
    does not work*/
    let categoryLine = document.createElement("p");
    let cat = document.createTextNode(toDo["taskCategory"]);
    categoryLine.appendChild(cat);
    note.appendChild(categoryLine);

    /*checkbox which will make the note disappear if checked,
    does not disappear yet*/
    let checkLine = document.createElement("p");
    let check = document.createElement("input");
    check.setAttribute("type", "checkbox");
    checkLine.appendChild(check);
    note.appendChild(checkLine);

    //adds the new section to the window, works
    let sec = document.getElementById('article');
    sec.appendChild(note);
}